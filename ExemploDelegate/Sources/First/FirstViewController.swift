//
//  FirstViewController.swift
//  ExemploDelegate
//
//  Created by Lucas Gomes on 08/02/23.
//

import UIKit

final class FirstViewController: UIViewController {

    @IBOutlet weak var textLabel: UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func nextButton(_ sender: UIButton) {
        let secondViewController = SecondViewController()
        secondViewController.delegate = self
        navigationController?.pushViewController(secondViewController, animated: true)
        print("next button")
    }
}

extension FirstViewController: SecondViewDelegate {
    func fieldCompleted(_ value: String) {
        guard let label = textLabel else { return }
        label.text = value
    }
}
