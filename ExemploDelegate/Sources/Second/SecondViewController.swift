//
//  SecondViewController.swift
//  ExemploDelegate
//
//  Created by Lucas Gomes on 08/02/23.
//

import UIKit

protocol SecondViewDelegate: AnyObject {
    func fieldCompleted(_ value: String)
}

final class SecondViewController: UIViewController {
    
    @IBOutlet weak var textTextField: UITextField?
    weak var delegate: SecondViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func returnButton(_ sender: UIButton) {
        delegate?.fieldCompleted(getTextFieldValue(textTextField))
        navigationController?.popViewController(animated: true)
    }
    
    private func getTextFieldValue(_ textField: UITextField?) -> String {
        guard let text = textField?.text, !text.trimmingCharacters(in: .whitespaces).isEmpty else { return "O texto vai aparecer aqui!" }
        return text
    }
}
