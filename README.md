<!-- Improved compatibility of back to top link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->
<a name="readme-top"></a>
<br />

<!-- ## Teste -->

<div align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="https://gitlab.com/uploads/-/system/project/avatar/43417971/git_project_logos.png?width=650" alt="Logo" width="200" height="200">
  </a>

### Delegate Exemplo

  <p align="center">
    Exemplo simples de como o usar o delegate em um projeto swift.
    <br />
    <a href="https://gitlab.com/users/dev-lucas/projects"><strong>Ver mais projetos »</strong></a>
    <br />
    <br />
  </p>
</div>

<details>
  <summary>Sumário</summary>
  <ol>
    <li>
      <a href="#sobre-o-projeto">Sobre o projeto</a>
    </li>
    <li>
      <a href="#como-executar-o-projeto">Como executar o projeto</a>
      <ul>
        <li><a href="#pré-requisitos">Pré-requisitos</a></li>
        <li><a href="#instalação">Instalação</a></li>
      </ul>
    </li>
    <li><a href="#contato">Contato</a></li>
  </ol>
</details>

<br>

---

<br>

### Sobre o Projeto

O projeto consiste em duas telas. Quando o usuário clica em "voltar" na segunda tela, uma validação é realizada para verificar se o text field foi preenchido. Em seguida, o usuário é redirecionado de volta para a primeira tela, onde o texto inserido é aplicado.

Primeira tela | Segunda tela
--- | ---
![](https://i.postimg.cc/HnVzTMgY/Simulator-Screen-Shot-i-Phone-14-Pro-2023-02-13-at-18-34-10.png) | ![](https://i.postimg.cc/C5XN5Khj/Simulator-Screen-Shot-i-Phone-14-Pro-2023-02-13-at-18-34-42.png)

O que voce pode encontrar nos códigos:
* Função para validar um textField vazio.
* Configuração básica para definir a view principal.
* Uso básico de constraint.


<br>

<div align="right">(<a href="#delegate-exemplo">voltar para o início</a>)</div>

---

<br>


### Como executar o projeto

Este é um exemplo de como você pode executar o projeto localmente.
Para colocar uma cópia local em funcionamento, siga as etapas abaixo.

#### Pré-requisitos

Este é um exemplo dos pré-requisitos que você precisa ter

- Xcode 11.1
- Este aplicativo está usando Swift 5

#### Instalação

Este é um exemplo de como instalar o projeto

1. Clonar o repositório
   ```
   git clone https://gitlab.com/dev-lucas/delegate-exemplo
   ```

<br>

<div align="right">(<a href="#delegate-exemplo">voltar para o início</a>)</div>

---

<br>


### Contato

Lucas Gomes Vieira - lucasgomes114@gmail.com

Link do projeto: [https://gitlab.com/dev-lucas/delegate-exemplo](https://gitlab.com/dev-lucas/delegate-exemplo)

<div align="right">(<a href="#delegate-exemplo">voltar para o início</a>)</div>

---

<br>
